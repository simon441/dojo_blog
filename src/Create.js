import { useState } from 'react'
import { useHistory } from 'react-router-dom'

const Create = () => {
  const [title, setTitle] = useState('')
  const [body, setBody] = useState('')
  const [author, setAuthor] = useState('mario')
  const [isPending, setIsPending] = useState(false)
  const history = useHistory()

  /**
   *
   * @param {Event} e
   */
  const handleSubmit = e => {
    e.preventDefault()

    const blog = { title, body, author }
    setIsPending(true)

    fetch('http://localhost:8000/blogs', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(blog),
    })
      .then(res => res.json())
      .then(data => {
        console.log(data)
        setIsPending(false)
        history.push('/')
      })
      .catch(err => {
        console.error(err)
        setIsPending(false)
        history.push('/')
      })
  }

  return (
    <div className="create">
      <h1>Add a New Blog</h1>
      <form onSubmit={handleSubmit}>
        <label htmlFor="title">Blog title:</label>
        <input id="title" type="text" required value={title} onChange={e => setTitle(e.target.value)} />
        <label htmlFor="body">Blog body:</label>
        <textarea id="body" required value={body} onChange={e => setBody(e.target.value)}></textarea>
        <label htmlFor="author">Blog author:</label>
        <select name="author" id="author" required value={author} onChange={e => setAuthor(e.target.value)}>
          <option value="mario">Mario</option>
          <option value="luigi">Luigi</option>
        </select>
        {!isPending && <button>Add Blog</button>}
        {isPending && <button disabled>Adding Blog...</button>}
      </form>
    </div>
  )
}

export default Create
