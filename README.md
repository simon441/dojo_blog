# Full React Tutorial

by The Net Ninja

1. Full React Tutorial #1 - Introduction
2. Full React Tutorial #2 - Creating a React Application
3. Full React Tutorial #3 - Components & Templates
4. Full React Tutorial #4 - Dynamic Values in Templates
5. Full React Tutorial #5 - Multiple Components
6. Full React Tutorial #6 - Adding Styles
7. Full React Tutorial #7 - Click Events
8. Full React Tutorial #8 - Using State (useState hook)
9. Full React Tutorial #9 - Intro to React Dev Tools
10. Full React Tutorial #10 - Outputting Lists
11. Full React Tutorial #11 - Props
12. Full React Tutorial #12 - Reusing Components
13. Full React Tutorial #13 - Functions as Props
14. Full React Tutorial #14 - useEffect Hook (the basics)
15. Full React Tutorial #15 - useEffect Dependencies
16. Full React Tutorial #16 - Using JSON Server
17. Full React Tutorial #17 - Fetching Data with useEffect
18. Full React Tutorial #18 - Conditional Loading Message
19. Full React Tutorial #19 - Handling Fetch Errors
20. Full React Tutorial #20 - Making a Custom Hook
21. Full React Tutorial #21 - The React Router
22. Full React Tutorial #22 - Exact Match Routes
23. Full React Tutorial #23 - Router Links
24. Full React Tutorial #24 - useEffect Cleanup
25. Full React Tutorial #25 - Route Parameters
26. Full React Tutorial #26 - Reusing Custom Hooks
27. Full React Tutorial #27 - Controlled Inputs (forms)
28. Full React Tutorial #28 - Submit Events
29. Full React Tutorial #29 - Making a POST Request
30. Full React Tutorial #30 - Programmatic Redirects
31. Full React Tutorial #31 - Deleting Blogs
32. Full React Tutorial #32 - 404 Pages & Next Steps

## How to use

- in one terminal, start the backend db: ```npx json-server --watch data/db.json --port 8000```
- in another terminal, start the react app: ```yarn start ```


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `yarn build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
